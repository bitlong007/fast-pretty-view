import { createApp } from "vue"
import ElementPlus from "element-plus"
import router from '~/router'

import 'element-plus/dist/index.css'

import "~/assets/style/index.css"

// If you want to use ElMessage, import it.
//import "element-plus/theme-chalk/src/message.scss"

import App from "./App.vue"
import { createPinia } from "pinia";

const pinia = createPinia();
const app = createApp(App);

app.use(ElementPlus, { size: 'small', zIndex: 3000 });
app.use(pinia);
app.use(router);

import * as ElementPlusIconsVue from '@element-plus/icons-vue'
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
  
app.mount("#app");
