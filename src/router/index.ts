import {useRoute, createRouter, createWebHashHistory} from 'vue-router'
import Vue from 'vue'

const routes = [
    {
        path: "/",
        redirect: 'Home'
    },
    {
        path: "/Home",
        name: "Home",
        component: () => import("../views/Home.vue")
    },
    {
        path: '/About',
        name: 'About',
        components: {
            default: () => import("../views/About.vue"),
            nav: () => import("../views/LeftMenu2.vue"),
        },
        meta: {
        }
    }, 
    {
        path: "/Application",
        name: "Application",
        components: {
           default: () => import("../views/Application.vue"),
           nav: () => import("../views/LeftMenu.vue")
        }
    }, 
    {
        path: "/Monitor",
        name: "Monitor",
        components: {
            default: () => import("../views/monitor/Home.vue"),
            nav: ()=> import("../views/monitor/LeftMenu.vue"),
        }
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
