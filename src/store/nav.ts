import { defineStore } from 'pinia'
import {ref} from 'vue';
//做一些其他的事情

//导出store
const nav = defineStore("nav", () => {
    const count2 = ref(0);

    function increment() {
        count2.value++;
        console.log("this is nav's increment method, result is :" + count2.value);
    }
    
    return {count2, increment}
});

export default nav
