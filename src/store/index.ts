import { defineStore, storeToRefs } from 'pinia'
import {ref} from 'vue'
import nav from '~/store/nav'

//做一些其他的事情

const my = defineStore("my", () => {
    const count = ref(0);

    function test() {
        count.value++;
        console.log("this is index.ts's test method, result = " + count.value);
        
        //const nav2 = nav();
        //nav2.increment();
    }

    return {test, count}
});

//导出store
export default defineStore("index", () => {

        const nav2 = nav();
        const {count2} = storeToRefs(nav2);

        const my2 = my();
        const {count} = storeToRefs(my2);

        return {my2, nav2 , count, count2}
});

